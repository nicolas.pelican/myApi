var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var cardSchema = new Schema({
    id: Number,
    firstname: String,
    lastname: String,
    alias: String,
    description: String,
    ptAtq: Number,
    ptDef: Number,
});

var CardModel = mongoose.model('cards', cardSchema);