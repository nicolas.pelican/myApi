
module.exports = function(app, db) {
    var cards = require('../controllers/cardController');

    //get all card
    app.get('/cards', cards.findAll);

    //get On card
    app.get("/card/:id", cards.findOne);

    //add card
    app.post("/card/add", function(req, res){
        //cards.add
        cards.add(req, res);
        console.log("add");
    });

}