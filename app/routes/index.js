// routes/index.js

const noteRoutes = require('./note_routes');
const userRoutes = require('./user_routes');
const cardRoutes = require('./card_routes');

module.exports = function(app, db) {
  userRoutes(app, db);
  noteRoutes(app, db);
  cardRoutes(app, db);

  // Other route groups could go here, in the future
};