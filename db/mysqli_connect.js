var mysql 		 = require('mysql'); 

var connection = mysql.createConnection ({
	host: 		"127.0.0.1",
	user: 		'root',
	password: 	'',
	post: 		3306,
	database: 	'mybazar',
});

connection.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
});

module.exports = connection;