
// server.js

const express        = require('express');
//const MongoClient    = require('mongodb').MongoClient;
const mongoose    = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
//var mongodb = require("mongodb");
const app            = express();
const bodyParser     = require('body-parser');

var db;


require('./app/models/cardModels');



//var mysql 		 = require('mysql');
var cors = require('cors');


var url = "mongodb://localhost:27017/angularbazar";


mongoose.Promise = global.Promise;

var connection = mongoose.connect(url, {
  useMongoClient: true,
});

autoIncrement.initialize(connection);



mongoose.connection.on('error', function(error) {
  console.error('Database connection error:', error);
});

db = mongoose.connection.once('open', function() {
  console.log('Database connected');
});




app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  next();
});

const port = 8001;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json() );
/*
app.use(express.json() );
*/
//app.use(bodyParser.text({ type :'text/html' }));


require('./app/routes')(app, db);

app.listen(port, () => {
  console.log('We are live on ' + port);
});

